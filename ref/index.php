<?php
error_reporting(0);
require_once("_config.php");
$action = $_GET['action'];
mysql_pconnect($_CONFIG['db_host'], $_CONFIG['db_login'], $_CONFIG['db_password']);
mysql_select_db($_CONFIG['db_table']);

// print_r($_GET);
// print_r($_POST);

$return = array(
	'success' => false,
	'message' => '',
	'data' => array()
);
switch($action){
	case 'check_email_exists':
		// check if the email already exist 
		if(check_email_exists($_GET['email'])) $return['success'] = true;
	break;
	case 'register_email':
		if(check_email_exists($_GET['email'])){
			$return['success'] = false;
			$return['message'] = 'Email '.$_GET['email'].' already exists';
		}else{
			$ref_code = substr(md5(time()), rand(0,26), 6);
			$checkspot_code = 
				substr(md5(microtime()), rand(0,24), 8) .'-'.
				substr(md5(microtime()), rand(0,28), 4) .'-'. 
				substr(md5(microtime()), rand(0,28), 4) .'-'. 
				substr(md5(microtime()), rand(0,28), 4) .'-'. 
				substr(md5(microtime()), rand(0,21), 11);
			$rank = get_rank('max') + 1;

			$ref_info = get_ref_by_refcode($_GET['referal']);

			$sql = "INSERT INTO `referals` SET 
					`ref_id` = ".intval($ref_info['id']).",
					`email` = '".addslashes($_GET['email'])."',
					`type` = '".addslashes($_GET['type'])."',
					`rank` = '".intval($rank)."',
					`current_rank` = '".intval($rank)."',
					`ref_code` = '".addslashes($ref_code)."',
					`checkspot_code` = '".addslashes($checkspot_code)."',
					`date_of_registration` = NOW()
			";
			mysql_query($sql);

			if($ref_info['id']){
				change_rank($ref_info['id'], "raise");
			}
			
			$return['success'] = true;
			$return['data'] = array(
				"ref_id" => addslashes($_GET['referal']),
				"email" => addslashes($_GET['email']),
				"type" => addslashes($_GET['type']),
				"rank" => intval($rank),
				"ref_code" => addslashes($ref_code),
				"checkspot_code" => addslashes($checkspot_code)
			);
			sendmail($_GET['email'], "register_email", $return['data']);
			setcookie('checkspot_code', $checkspot_code, 0, '/');

			require_once("mailchimp-api-php/MCAPI.class.php");
			$api = new MCAPI($_CONFIG['MC_API_KEY']);
			$merge_vars = array(
				"MMERGE3" => ($_GET['type']=='Trader'?'Peer Trader':'Market Maker'),
				"REF_CODE" => $ref_code,
				"CHECK_SPOT" => $checkspot_code,
			);
			$retval = $api->listSubscribe( $_CONFIG['MC_LIST_ID'], $_GET['email'], $merge_vars, false, false);
		}
	break;
	case 'get_info':
		$return['success'] = true;
		$return['data'] = get_ref_by_checkspot_code($_GET['checkspot_code']);
	break;
	case 'resend_email':
		$return['success'] = true;
		$data = get_ref_by_email($_GET['email']);
		sendmail($_GET['email'], "resend_email", $data);
	break;
	case 'get_spot':
		$ref = get_ref_by_checkspot_code($_GET['checkspot_code']);
		$return['success'] = true;
		$return['data'] = get_real_rank($ref['id']);
	break;
	case 'get_shares':
		$ref = get_ref_by_checkspot_code($_GET['checkspot_code']);
		$return['success'] = true;
		$return['data'] = get_share_rank($ref['id']);
	break;

}

mysql_close();

header('Content-Type: application/json');
die(json_encode($return));



function check_email_exists($email = ''){
	$sql = "SELECT * FROM `referals` WHERE `email`='".addslashes($email)."'";
	if(mysql_num_rows(mysql_query($sql))) return true;
	else return false;
}

function get_ref_by_refcode($refcode = ''){
	$sql = "SELECT * FROM `referals` WHERE `ref_code` = '".addslashes($refcode)."' LIMIT 0,1";
	return mysql_fetch_assoc(mysql_query($sql));
}

function get_ref_by_checkspot_code($checkspot_code = ''){
	$sql = "SELECT * FROM `referals` WHERE `checkspot_code` = '".addslashes($checkspot_code)."' LIMIT 0,1";
	return mysql_fetch_assoc(mysql_query($sql));
}

function get_ref_by_email($email = ''){
	$sql = "SELECT * FROM `referals` WHERE `email` = '".addslashes($email)."' LIMIT 0,1";
	return mysql_fetch_assoc(mysql_query($sql));
}

function get_real_rank($id){
	$sql = "SELECT `rank`, `current_rank` FROM `referals` WHERE `id`=".intval($id);
	$curent = mysql_fetch_assoc(mysql_query($sql));

	$sql = "SELECT COUNT(`id`) as `ahead` FROM `referals` WHERE `current_rank`<".$curent['current_rank']." || (`current_rank`=".$curent['current_rank']." && `rank`>".$curent['rank'].")";
	$ahead = mysql_fetch_assoc(mysql_query($sql));
	
	$sql = "SELECT COUNT(`id`) as `behind` FROM `referals` WHERE `current_rank`>".$curent['current_rank']." || (`current_rank`=".$curent['current_rank']." && `rank`<".$curent['rank'].")";
	$behind = mysql_fetch_assoc(mysql_query($sql));

	return array(
		"ahead" => $ahead['ahead'],
		"behind" => $behind['behind'],
	);
}

function get_share_rank($id, $limit = 10){
	$sql = "SELECT `no_of_share`,`date_of_registration` FROM `referals` WHERE `id`=".intval($id);
	$self = mysql_fetch_assoc(mysql_query($sql));

	$sql = "SELECT `id`, `no_of_share`, `date_of_registration` FROM `referals` WHERE `id`!=".intval($id)." && (`no_of_share`>".$self['no_of_share']." || (`no_of_share`=".$self['no_of_share']." && `date_of_registration`<'".$self['date_of_registration']."')) ORDER BY `no_of_share` ASC, `date_of_registration` DESC";
	$self_share_rank_res = mysql_query($sql);
	$self_share_rank = (mysql_num_rows($self_share_rank_res) + 1);
	
	$above_share_rank = 0;
	if($self_share_rank>1){
		$above_share_info = mysql_fetch_assoc($self_share_rank_res);
		$need_more_shares_to_get_above = ($above_share_info['no_of_share'] - $self['no_of_share'] + 1);

		$above_share_info1 = mysql_fetch_assoc($self_share_rank_res);
		if($above_share_info1['no_of_share'] == $need_more_shares_to_get_above && convert_datetime2timestamp($above_share_info1['date_of_registration']) < convert_datetime2timestamp($self['date_of_registration'])){
			$need_more_shares_to_get_above++;
		}
		$above_share_rank = $need_more_shares_to_get_above;

		$sql = "SELECT `id`, `no_of_share`, `date_of_registration` FROM `referals` WHERE (`no_of_share`>".($self['no_of_share']+$need_more_shares_to_get_above)." || (`no_of_share`=".($self['no_of_share']+$need_more_shares_to_get_above)." && `date_of_registration`<'".$self['date_of_registration']."')) ORDER BY `no_of_share` ASC, `date_of_registration` DESC";
		$willbe_share_rank_res = mysql_query($sql);
		$willbe_share_rank = (mysql_num_rows($willbe_share_rank_res) + 1);
	}
	/*
	if($self_share_rank>1){
		$above_share_info = mysql_fetch_assoc($self_share_rank_res);
		$need_more_shares_to_get_above = ($above_share_info['no_of_share'] - $self['no_of_share'] + 1);

		$above_share_info1 = mysql_fetch_assoc($self_share_rank_res);
		if($above_share_info1['no_of_share'] == $need_more_shares_to_get_above && convert_datetime2timestamp($above_share_info1['date_of_registration']) < convert_datetime2timestamp($self['date_of_registration'])){
			$need_more_shares_to_get_above++;
		}
		$above_share_rank = $need_more_shares_to_get_above;
	}
	*/

	$sql = "SELECT * FROM `referals` WHERE 1 ORDER BY `no_of_share` DESC LIMIT 0,".(intval($limit)>0 ? intval($limit) : 10);
	$res = mysql_query($sql);


	$return = array(
		"top" => array(),
		"self" => $self['no_of_share'],
		"self_share_rank" => $self_share_rank,
		"above_share_rank" => $above_share_rank,
		"willbe_share_rank" => $willbe_share_rank,
	);
	while($row = mysql_fetch_assoc($res)){
		$return['top'][] = $row;
	}

	return $return;
}


function get_rank($cond){
	if(is_numeric($cond) && intval($cond)>0){
		// get rank of an ID
		$sql = "SELECT `current_rank` FROM `referals` WHERE `id`=".intval($cond);
		$res = mysql_fetch_assoc(mysql_query($sql));
		return $res['current_rank'];
	}
	if($cond == 'max'){
		// get max rank
		$sql = "SELECT COUNT(`id`) as `maxrank` FROM `referals` WHERE 1";
		$res = mysql_fetch_assoc(mysql_query($sql));
		return $res['maxrank'];
	}
}
function get_number_of_refs($id){
	$sql = "SELECT COUNT(`id`) as `number_of_refs` FROM `referals` WHERE `ref_id`=".intval($id);
	$res = mysql_fetch_assoc(mysql_query($sql));
	return $res['number_of_refs'];
}

function change_rank($id, $action){
	$current_rank = get_rank($id);

	if($action=='lower'){
		$new_rank = $current_rank + 1;
		$change_rank_sign = "-";
	}

	if($action=='raise'){
		$new_rank = $current_rank - 1;
		$change_rank_sign = "+";
		$no_of_share_add = " , `no_of_share` = (`no_of_share`+1) ";
	}

	$sql = "UPDATE `referals` SET `current_rank` = ".$new_rank.$no_of_share_add." WHERE `id`=".$id;
	mysql_query($sql);

	// $sql = "UPDATE `referals` SET `current_rank` = (`current_rank`".$change_rank_sign."1) WHERE `current_rank` = ".$new_rank." && `id`!=".$id;
	// mysql_query($sql);
}

function sendmail($aim, $template_name, &$info)
{
	global $_CONFIG, $_EMAIL_CODES;
    if(!$aim) return 0;
    $text = split("\n", file_get_contents("email_tpl/".$template_name.".html"));
    $subject = array_shift($text);
    $text = join("\n", $text);

    $originalsubj = $subject;

    $boundary     = "--".md5(uniqid(time())); 
    $EOL = "\r\n";
    $charset = "utf-8";
    $from = $_CONFIG['email_from'];

    
    foreach ($info as $key => $value) {
    	$text = str_replace("*|ref_".$key."|*", $value, $text);
    }
    foreach($_EMAIL_CODES as $code => $value){
    	$text = str_replace("*|".$code."|*", $value, $text);
    }

	$text = str_replace("*|URL:ARCHIVE_LINK_SHORT|*", "http://".$_SERVER['HTTP_HOST']."/?ref=".$info['ref_code'], $text);
	$text = str_replace("*|MC:SUBJECT|*", $subject, $text);


    $subject="=?".$charset."?B?".base64_encode($subject)."?=";

     //  $html ="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">
     //                <HTML><HEAD>
     //                <META content=\"MSHTML 6.00.2800.1106\" name=GENERATOR>
					// <meta http-equiv=\"Content-Type\" content=\"text/html; charset=".$charset."\" /> 
     //                <STYLE></STYLE>
     //                </HEAD>
     //                <BODY bgColor=#ffffff>
     //                ".$text."\n
     //                </BODY></HTML>";

    $html = $text;
    $headers  = "MIME-Version: 1.0;$EOL";
    $headers .= "Content-Type: multipart/mixed; charset=".$charset."; boundary=\"$boundary\"$EOL";
    $headers .= "From: ".$from."$EOL";
    $headers .= "X-Mailer: PHP$EOL";
    $headers .= "Content-Transfer-Encoding: 8bit$EOL";
    $headers .= "Mime-Version: 1.0";
    $headers .=  phpversion();

    $body  = "--$boundary$EOL";
    $body .= "Content-Type: text/html; charset=".$charset."$EOL";
    $body .= "Content-Transfer-Encoding: base64$EOL";
    $body .= $EOL;
    $body .= chunk_split(base64_encode($html));
    $body .=  "$EOL--$boundary$EOL";

    if (mail($aim, $subject,$body, $headers)) $result = true;
    else $result = false;

    return $result;
}//function sendmail($aim,$from,$subject,$text)

function convert_datetime2timestamp($datetime = '0000-00-00 00:00:00'){
	list($date, $time) = explode(" ", trim($datetime), 2);
	list($y, $m, $d) = explode("-", trim($date), 3);
	list($h, $i, $s) = explode(":", trim($time), 3);

	return mktime($m, $d, $y, $h, $i, $s);
}
?>