-- phpMyAdmin SQL Dump
-- version 3.4.10.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 05, 2014 at 11:17 PM
-- Server version: 5.0.92
-- PHP Version: 5.3.24

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dev_pd`
--

-- --------------------------------------------------------

--
-- Table structure for table `referals`
--

CREATE TABLE IF NOT EXISTS `referals` (
  `id` bigint(20) NOT NULL auto_increment,
  `ref_id` bigint(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `type` enum('Trader','MarketMaker') NOT NULL default 'Trader',
  `rank` bigint(20) NOT NULL,
  `current_rank` int(11) NOT NULL,
  `no_of_share` bigint(20) NOT NULL,
  `ref_code` varchar(65) NOT NULL,
  `checkspot_code` varchar(35) NOT NULL,
  `date_of_registration` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;