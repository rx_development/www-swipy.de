$(function() {
	var url = '/ref/';
		
	$("#get-access").on('click', function(){
		var email = $("input[name='ref-email']").val();
		var type = $("select[name='ref-type']").val();
		var referal = getParameterByName('ref');
		// alert(referal);
		if(!email){
			rx_returnerror_noemail();
			return false;
		}
		if(!type){
			rx_returnerror_notype();
			return false;
		}

		data = {
			'action' : 'register_email',
			'email' : email,
			'type' : type,
			'referal' : referal
		};
		$.ajax({
			url: url,
			data: data,
			dataType: 'JSON',
			beforeSend: function(){},
			success: function(data){
				if(!data.success){ // if it is true than the provided email does exist already
					$("#ref_messagefield").html("You're already on the waitlist!<br/>Check your email for a link to view your spot in line.<br/><span id='rx_email_sent'>Can't find the email? <a href='#' id='rx_resendemail'>Click here</a> to re-send the link.</span>");
					$("#rx_resendemail").on('click', function(){
						rx_resend_email(email);
					});
				}else{ // if the email does not exist than register the new one
					// console.log(data.data);
					  document.cookie = "checkspot_code=" + encodeURIComponent(data.data.checkspot_code);

					location.href = '?checkspot_code='+data.data.checkspot_code;
				}
			}
		});
		return true;
	});

	function rx_returnerror_noemail(){
		alert('please provide email');
	}
	function rx_returnerror_notype(){
		alert('please provide type');
	}
	function rx_resend_email(email){
		data = {
			'action' : 'resend_email',
			'email' : email,
		};
		$.ajax({
			url: url,
			data: data,
			dataType: 'JSON',
			beforeSend: function(){},
			success: function(data){
				$("#rx_email_sent").fadeOut("slow", function(){
					$(this).text("Email sent.");
					$(this).fadeIn();
				});
			}
		});

	}
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
});