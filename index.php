<!DOCTYPE html>
<!--[if lte IE 8]>               <html class="ie8 no-js" lang="en">    <![endif]-->
<!--[if lte IE 10]>				 <html class="ie10 no-js" lang="en">   <![endif]-->
<!--[if !IE]>-->
<html class="not-ie no-js" lang="en">
<!--<![endif]-->
<head>

<!-- Google Web Fonts
  ================================================== -->
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,700|Julius+Sans+One|Roboto+Condensed:300,400|Homenaje' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>

<!-- Basic Page Needs
  ================================================== -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Swipy</title>
<meta name="description" content="">
<meta name="author" content="">
 
<!-- Favicons
	================================================== -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="icon" type="image/x-icon" href="images/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
<link type="image/x-icon" href="images/favicon.ico">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
  ================================================== -->
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/grid.css" />
<link rel="stylesheet" href="css/layout.css" />
<link rel="stylesheet" href="css/fontello.css" />
<link rel="stylesheet" href="css/animation.css" />
<link rel="stylesheet" href="css/rx.css.css" />
<link rel="stylesheet" href="js/layerslider/css/layerslider.css" />
<link rel="stylesheet" href="js/flexslider/flexslider.css" />
<link rel="stylesheet" href="js/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" href="js/owl-carousel/owl.theme.css" />

<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" />
<link rel="stylesheet" href="js/video-js/video-js.css" />

<!-- HTML5 Shiv
	================================================== -->
<script src="js/jquery.modernizr.js"></script>
</head>

<body data-spy="scroll" data-target="#navigation" class="home">

<!-- - - - - - - - - - - - - - BG Video - - - - - - - - - - - - - - - - -->

<!--
<video id="example_video" class="video-js vjs-default-skin vjs-fullscreen" width="1280" height="720" >
  <source src="media/swipe-vid-2.mp4" type='video/mp4' />
</video>
-->

<!-- - - - - - - - - - - - - end BG Video - - - - - - - - - - - - - - - --> 

<!-- - - - - - - - - - - - - - Loader - - - - - - - - - - - - - - - - -->

<div class="loader"></div> 
<!--/ .loader--> 

<!-- - - - - - - - - - - - - end Loader - - - - - - - - - - - - - - - --> 

<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

<header id="header" class="transparent">
  <div class="header-in clearfix">
    <h1 id="logo"><a href="index.html"><img src="images/logo.png" alt="Swipy" /></a></h1>
    <a id="responsive-nav-button" class="responsive-nav-button" href="#"></a>
    <nav id="navigation" class="navigation">
      <ul>
        <li class="current-menu-item"><a href="#home">Startseite</a></li>
        <li><a href="#faq">Wie funktioniert swipy?</a></li>
        <li><a href="#press">Presse</a></li>
        <li><a href="#contacts">Kontakt</a></li>
        <li><a href="#policy">Datenschutz</a></li>
        <li><a href="#imprint">Impressum</a></li>

        <li class="social-link facebook"><a href="http://facebook.com/swipyde"><i class="icon-facebook"></i></a></li>
        <li class="social-link twitter"><a href="http://twitter.com/swipyshopping"><i class="icon-twitter"></i></a></li>
        <li class="social-link instagram"><a href="http://instagram.com/swipyshopping"><i class="icon-instagramm"></i></a></li>

      </ul>
    </nav>
    <!--/ .social-icons-->
    <!--/ #navigation--> 
    
  </div>
  <!--/ .header-in--> 
  
</header>
<!--/ #header--> 

<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - --> 

<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->

<div id="wrapper">
  <section id="home" class="page">
    <section class="section parallax padding-off">

      <div id="layerslider-container">
        <div id="layerslider">
          <div class="ls-layer" style="slidedirection: right; durationin: 1500; durationout: 1500; easingin: easeInOutQuint;background:#cfcfcf">
            
          </div>
          
          <div class="align-center get-started" id="ref_messagefield" style="<?echo $_GET['checkspot_code']?'color:#000;':''?>">  
            <?if($_GET['checkspot_code']){
              $info = json_decode(file_get_contents("http://".$_SERVER['HTTP_HOST']."/ref/?action=get_info&checkspot_code=".$_GET['checkspot_code']), true);
              $info = $info['data'];
              $spot = json_decode(file_get_contents("http://".$_SERVER['HTTP_HOST']."/ref/?action=get_spot&checkspot_code=".$_GET['checkspot_code']), true);
              $spot = $spot['data'];
              $shares = json_decode(file_get_contents("http://".$_SERVER['HTTP_HOST']."/ref/?action=get_shares&checkspot_code=".$_GET['checkspot_code']), true);
              $shares = $shares['data'];
              //echo '<pre>';print_r($info);echo '</pre>';
              // echo "!!!!! -> http://".$_SERVER['HTTP_HOST']."/ref/?action=get_shares&checkspot_code=".$_GET['checkspot_code'];

              $share_url = "http://".$_SERVER['HTTP_HOST']."/?ref=".$info['ref_code'];
              $share_text = "Robinhood is a commission-free stock brokerage. Reserve your #zerocommission portfolio today @via=robinhoodapp";
              $email_text = "Robinhood is a commission-free stock brokerage. Reserve your portfolio today at ".$share_url;
              $email_subject = "Check out this new stock brokerage";
              ?>
              <div>
                <?=$spot['ahead']?> People ahead of you</br>
                <?=$spot['behind']?> People behind you
              </div>
              <div>This reservation is held for <?=$info['email']?>. Is this <a href="?new">not you?</a></div>
              <div>
                Interested in priority access?<br/>
                Get early access by referring your friends. The more friends that join, the sooner you'll get access.
              </div>

              
              <div class="mbl">
              <div class="share-wrapper">
                <a class="btn btn-default btn-lg share-button" href="https://twitter.com/intent/tweet?url=<?=rawurlencode($share_url)?>&amp;text=<?=rawurlencode($share_text)?>" id="share_twitter" target="_blank">
                  <img alt="Twitter" class="share-icon" src="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/twitter.png" data-at2x="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/twitter%402x.png">
                  Tweet
                </a>
              </div>

              <div class="share-wrapper">
                <a class="btn btn-default btn-lg share-button" href="https://www.facebook.com/sharer/sharer.php?u=<?=rawurlencode($share_url)?>" id="share_facebook" target="_blank">
                  <img alt="Facebook" class="share-icon" src="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/facebook.png" data-at2x="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/facebook%402x.png">
                  Share
                </a>
              </div>

              <div class="share-wrapper">
                <a class="btn btn-default btn-lg share-button" href="mailto:?body=<?=rawurlencode($email_text)?>&amp;subject=<?=rawurlencode($email_subject)?>" id="share_email" target="_blank">
                  <img alt="Email" class="share-icon" src="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/email.png" data-at2x="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/email%402x.png">
                  Email
                </a>
              </div>

              <div class="share-wrapper">
                <a class="btn btn-default btn-lg share-button" href="https://www.linkedin.com/cws/share?url=<?=rawurlencode($share_url)?>" id="share_linkedin" target="_blank">
                  <img alt="LinkedIn" class="share-icon" src="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/linkedin.png" data-at2x="https://brokerage-static.s3.amazonaws.com/assets/marketing/images/waitlist/linkedin%402x.png">
                  Share
                </a>
              </div>

              <div class="lead mts" id="referral_header">
                Or share this unique link:
              </div>
              <div id="referral_url"><?=$share_url?></div>
            </div>

            <div><?//echo '<pre>';print_r($shares);echo '</pre>';?>
            Hi, You have <?=$shares['self']?> Shares, your current rank is: <?=$shares['self_share_rank']?>. You need <?=$shares['above_share_rank']?> more shares to be rank <?=$shares['willbe_share_rank']?>. Go and Share!
              <div>TOP10 sharers:</div>
              <div align="center"><table>
                <?
                $prize_values = array(
                	"1" => "1 000 000",
                	"2" => "500 000",
                	"3" => "250 000",
                	"4" => "100 000",
                	"5" => "50 000",
                	"6" => "25 000",
                	"7" => "15 000",
                	"8" => "10 000",
                	"9" => "5 000",
                	"10" => "1 000",
                	);
                
                foreach($shares['top'] as $k => $sh_name){
					$sh_spot = $k+1;
                	?>
                <tr>
                <td><?echo ($sh_spot!=$shares['self_share_rank']) ? $sh_spot : "YOU"?></td>
                <td><?=$sh_name['no_of_share']?> shares</td>
                <td><?=$prize_values[$sh_spot]?></td>
                </tr>
                <?}?>
              </table></div>
            </div>


            <?}else{
              if($_COOKIE['checkspot_code'] && !isset($_GET['new'])){?>
              <a class="button large opacityRun" href="?checkspot_code=<?=$_COOKIE['checkspot_code']?>">Check Your Spot</a>
              <?}else{
              ?>
            <input type="text" value="" placeholder="your e-mail" name="ref-email" class="rx_ref_input" style="color:#000"/>&nbsp;
            <select name="ref-type" class="rx_ref_select">
              <option value="">you are:</option>
              <option value="Trader">Trader</option>
              <option value="MarketMaker">MarketMaker</option>
            </select>
            <a id="get-access" class="button large opacityRun" href="#">Get early access</a>
            <?}?>

            <?}?>
          </div>
          <!--/ .ls-layer-->
          




        </div>
      </div>
      <!--/ #layerslider-container-->
      <div class="home-shops">
        <div class="row">
          <div class='our-partners-wrapper'>
            <div class="our-partners">
              <div class="item"><a href="#"><img src="images/partners/Zalando_Logo.jpg" alt="Zalando" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/BettyBarclay_logo.jpg" alt="BettyBarclay" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/Buffalo_Logo.png" alt="Buffalo" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/fashionette.jpg" alt="Fashionette" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/fashion-id-logo.jpg" alt="Fashion ID" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/hunkemoller.jpg" alt="Hunkemöller" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/SOliverLogo.jpg" alt="SOliver" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/spartoo.jpg" alt="Spartoo" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/superdry%20logo.jpg" alt="Superdry" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/topman.jpg" alt="topman" /></a></div>   
              <div class="item"><a href="#"><img src="images/partners/snipes.jpg" alt="snipes" /></a></div>
              <div class="item"><a href="#"><img src="images/partners/monsoon.jpg" alt="monsoon" /></a></div>                                          


            </div>
            <div class="customNavigation">
              <a class="btn prev"></a>
              <a class="btn next"></a>
            </div>
          </div>

        </div>
      </div> <!-- / .home-shops -->
      <!--<div class="play-video visible-xs visible-sm"><a class="img-circle" data-fancybox-group="folio" href="media/swipe-vid-2.mp4"><i class="icon-play"></i> </a></div>-->
      <!--
			<ul class="keydown">
				<li class="up"></li>
				<li class="left"></li>
				<li class="down"></li>
				<li class="right"></li>
			</ul><!--/ .keydown--> 
      
    </section>
    <!--/ .section--> 
    
  </section>
  <!--/ .page-->

  <section id="faq" class="page">
    <section class="section padding-bottom-off">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <hgroup class="section-title align-center opacityRun">
              <h1>Wie funktioniert swipy?</h1>
            </hgroup>

          </div>
        </div>
        <div class="row">

          <img class="col-xs-6" src="images/how-it-works/iPhone.jpg" />
          <img class="col-xs-6" src="images/how-it-works/iPad.jpg" />
          <img class="col-xs-12" src="images/how-it-works/laptop.jpg" />

        </div>
        <div class="row">
          <h2 class="align-center center-block col-xs-10 orange-title">
            Einfach mit deinem Finger über den Smartphone/Tablet „wischen“ oder auf dem PC mit den Pfeiltasten  hin und her wechseln!
          </h2>
        </div>
        <!--/ .row-->
      </div>
      <!--/ .container-->

      <div class="container">

        <div class="acc-box">

          <span class="acc-trigger" data-mode=""><a href="#"><strong>Was ist Swipy?</strong></a></span>
          <div class="acc-container">
            <p>
              Swipy ist wohl die einfachste Art Mode & Accessoires zu finden und zu kaufen.
              Es kann als das Tinder für Mode bezeichnet werden. Unsere mobile Seite ist simpel,
              intuitiv und macht Spaß. Wir möchten Dir besonders das mühsame Stöbern durch die
              unzähligen Angebote aus Shops vereinfachen. Wir sind kein Shop, sondern haben Partnerschaften
              mit den großen und bekanntesten Shops für Mode.  Du wirst von uns weitergeleitet und die Bezahlung
              sowie den Versand erledigt Euer Lieblingsshop!
            </p>
          </div><!--/ .acc-container-->

          <span class="acc-trigger" data-mode=""><a href="#"><strong>Muss ich mich anmelden, etwas bezahlen oder vertrauliche Daten weitergeben?</strong></a></span>
          <div class="acc-container">
            <p>Swipy, das Tinder für Bekleidung, kannst Du kostenlos  & ohne Anmeldung über unsere mobile Seite nutzen.
              Du brauchst nichts herunterzuladen. Wir möchten auch keine vertraulichen Daten von Dir, da wir viel Wert auf Sicherheit legen!
              Swipy ist und bleibt komplett kostenlos.</p>
          </div><!--/ .acc-container-->

          <span class="acc-trigger" data-mode=""><a href="#"><strong>Wie funktioniert Swipy eigentlich?</strong></a></span>
          <div class="acc-container">
            <p>Am besten ist es, wenn du über ein Smartphone oder Tablet Swipy nutzt, da swipy Dich dann komplett begeistern wird!</p>

            <ul class="list circle-list opacityRun">
              <li>Wenn du mit deinem Finger nach rechts wischst („swipe“) kannst Du Dir ein Produkt merken und zu Deiner „Wischlist“ hinzufügen. Du kannst auch dann Freunden davon berichten und zeigen was Dir gefällt und es teilen!</li>
              <li>Wischst du nach links, kommt sofort ein neues passendes Angebot.</li>
              <li>Wenn Du etwas Passendes gefunden hast, dann klicke auf das Produkt, bekomme weitere Infos und nach dem Klick auf den Shop-Button wirst zu unseren Partner weitergeleitet.</li>
              <li>Klickst du auf den Warenkorb, so wirst du Direkt zum Partner auf das ausgewählte Produkt geleitet.</li>
            </ul>

            <p>Du kannst aber genauso einfach auf das „X“ oder auf das „Herz“ zum Liken oder Dis-liken klicken.</p>
          </div><!--/ .acc-container-->

          <span class="acc-trigger" data-mode=""><a href="#"><strong>Kann ich bei Swipy auch nach Geschlecht, Preisen, Kategorien oder Shops filtern?</strong></a></span>
          <div class="acc-container">
            <p>Ja! Wir möchten dass du einfach und mit Spaß shoppen kannst, aber auch eine Möglichkeit bieten,
              genau deine Wunschprodukte gezielter suchen zu können. Du kannst bei uns  unter dem Icon „Filter“ ,
              nach Geschlecht, Preisspanne, Kategorie und Shops filtern. Die Filter funktionieren jeweils einzeln
              und auch  in Kombination miteinander – versuche es einfach und lass dich von unserem Tinder für Mode begeistern!</p>
          </div><!--/ .acc-container-->

          <span class="acc-trigger" data-mode=""><a href="#"><strong>Wie wichtig ist Datenschutz bei swipy?</strong></a></span>
          <div class="acc-container">
            <p>Wir legen besonders wert auf Einfachheit und Sicherheit. Eure Privatspähre und Datenschutz liegen uns sehr am Herzen.Swipy soll als Tinder für Kleider einfach einfach sein! Wir verzichten auf blinkende Elemente oder auf nervige Pop-Ups. Wir wollen, dass Du uns gerne nutzt und uns auch hilfst noch besser zu werden.</p>
          </div><!--/ .acc-container-->


          <span class="acc-trigger" data-mode=""><a href="#"><strong>Wenn ich Tipps, Anregungen oder Kritik habe, darf ich mich an Euch wenden?</strong></a></span>
          <div class="acc-container">
            <p>Klar! Besuche einfach unsere Facebook oder Twitter Seite und melde Dich!<br />
              Du kannst uns aber auch gerne an feedback@swipy.de eine E-Mail senden. Wir freuen uns auf deine Meinung und antworten schnell!</p>
          </div><!--/ .acc-container-->

        </div><!--/ .acc-box-->
        <br /><br /><br /><br /><br />

      </div>
    </section>

<section class="section parallax parallax-bg-3 bg-turquoise-color hidden-xs hidden-sm">
      <div class="full-bg-image"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <ul>
              <li class="align-center">
                <blockquote class="quote-text">
                  <p>Swipy ist einfach ein genial! Liken, Disliken und kaufen, alles auf einen „Wisch“. So viel Spaß hatte ich noch nie beim Modekaufen!<br /><br /></p>
                </blockquote>
                <div class="quote-image"><img alt="Caro" src="images/testimonials/caro.jpg" /></div>
                <div class="quote-author"><span>Caro, Bloggerin</span></div>
              </li>
            </ul>
            <!--/ .quotes-->
          </div>
          
          <div class="col-xs-4">  
            <ul>        
              <li class="align-center">
                <blockquote class="quote-text">
                  <p>Ich habe noch nie so schnell und spielerisch nach Mode gesucht. Der Produktfilter und die Wischlist sind perfekt! Ich werde allen davon erzählen! Danke liebes Swipy Team!</p>
                </blockquote>
                <div class="quote-image"><img alt="Lena" src="images/testimonials/lena.jpg"></div>
                <div class="quote-author"><span>Lena, Marketing-Assistentin</span></div>
              </li>
            </ul>
          </div>
          
          <div class="col-xs-4">  
            <ul>                
              <li class="align-center">
                <blockquote class="quote-text">
                  <p>Swipy ist auch für modebewusste Männer eine praktische Sache. Ich hätte nie gedacht, dass mir Modeshopping im Internet so viel Spaß machen kann!<br /></p>
                </blockquote>
                <div class="quote-image"><img alt="Alex" src="images/testimonials/alex.jpg"></div>
                <div class="quote-author"><span>Alex, Student</span></div>
              </li> 
             </ul>
           </div>  
        </div>
        <!--/ .row-->

      </div>
      <!--/ .container-->

    </section>
    <!--/ .section-->

    <section class="section parallax parallax-bg-3 bg-turquoise-color hide visible-xs visible-sm">
      <div class="full-bg-image"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <ul class="quotes opacity" data-timeout="6000">
              <li class="align-center">
                <blockquote class="quote-text">
                  <p>Swipy ist einfach ein genial! Liken, Disliken und kaufen, alles auf einen „Wisch“. So viel Spaß hatte ich noch nie beim Modekaufen!</p>
                </blockquote>
                <div class="quote-image"><img alt="Caro" src="images/testimonials/caro.jpg" /></div>
                <div class="quote-author"><span>Caro, Bloggerin</span></div>
              </li>
              <li class="align-center">
                <blockquote class="quote-text">
                  <p>Ich habe noch nie so schnell und spielerisch nach Mode gesucht. Der Produktfilter und die Wischlist sind perfekt! Ich werde allen davon erzählen! Danke liebes Swipy Team!</p>
                </blockquote>
                <div class="quote-image"><img alt="Lena" src="images/testimonials/lena.jpg"></div>
                <div class="quote-author"><span>Lena, Marketing-Assistentin</span></div>
              </li>
              <li class="align-center">
                <blockquote class="quote-text">
                  <p>Swipy ist auch für modebewusste Männer eine praktische Sache. Ich hätte nie gedacht, dass mir Modeshopping im Internet so viel Spaß machen kann!</p>
                </blockquote>
                <div class="quote-image"><img alt="Alex" src="images/testimonials/alex.jpg"></div>
                <div class="quote-author"><span>Alex, Student</span></div>
              </li>             
              
              
            </ul>
            <!--/ .quotes-->

          </div>
        </div>
        <!--/ .row-->

      </div>
      <!--/ .container-->

    </section>
    <!--/ .section-->

    <section class="section parallax parallax-bg-4">
      <div class="full-bg-image"></div>
      <div class="parallax-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
          </div>
        </div>
        <!--/ .row-->
      </div>
      <!--/ .container-->

    </section>
    <!--/ .section-->
  </section>
  <!--/ .page-->
  





  
  <section id="press" class="page">
    <section class="section bg-gray-color">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <hgroup class="section-title align-center opacity">
              <h1>Presse</h1>
              <h3>Du möchtest über uns berichten, ein Interview führen oder benötigst weitere Infos?</h3>
              <h3>Schreibe einfach direkt an unseren Gründer: <a class="mail-link" href="mailto:ben@swipy.de">ben@swipy.de</a></h3>
            </hgroup>
          </div>
        </div>
        <!--/ .row-->
        <div class="row">
          <div class="col-xs-12">

          <h4 class="align-center">Wir haben auch schon etwas vorbereitet:</h4>
          <br /><br />
          <div class="align-center press-kit">
            <a class="button large opacityRun" target="_blank" href="http://www.swipy.de/press/presskit.rar"><i class="icon-download-6"></i> PRESS KIT</a>
          </div>



          </div>
        </div>
      </div>
      <!--/ .container--> 
      
    </section>
    <!--/ .section--> 
    
  </section>
  <!--/ .page-->

  <section id="contacts" class="page">
      <section class="section padding-bottom-off">
          <div class="container">
              <div class="row">
                  <div class="col-xs-12">
                      <hgroup class="section-title align-center opacity">
                          <h1>Kontakt</h1>
                          <h2>Du hast Fragen, Anregung oder Kritik? Schreib uns!</h2>
                      </hgroup>
                  </div>
              </div>
              <!--/ .row-->

          </div>
          <!--/ .container-->

      </section>
      <!--/ .section-->

      <!--/ .section-->
      <section class="section parallax parallax-bg-8">
          <div class="full-bg-image"></div>
        <div class="parallax-overlay"></div>
          <div class="container">
              <div class="row">
                  <div class="col-md-6 opacity">
                      <form class="contact-form" method="post" action="/">
                          <p class="input-block">
                              <input type="text" name="name" id="name" placeholder="Name *" />
                          </p>
                          <p class="input-block">
                              <input type="email" name="email" id="email" placeholder="E-Mail Adresse *" />
                          </p>
                          <p class="input-block">
                              <textarea name="message" id="message" placeholder="Nachricht *"></textarea>
                          </p>
                          <p class="input-block">
                            <iframe src="php/capcha_page.php" height="50" width="100" scrolling="no" frameborder="0" marginheight="0" marginwidth="0" class="capcha-frame" name="capcha_image_frame"></iframe>
                            <input class="verify" type="text" id="verify" name="verify" />
                          </p>
                          <p class="input-block">
                              <button class="button turquoise submit" type="submit" id="submit"><i class="icon-paper-plane-2"></i></button>
                          </p>

                      </form>
                      <!--/ .contact-form-->

                  </div>
                  <div class="col-md-6">
                      <div class="widget widget_text opacity">
                          <p> Angaben gemäß § 5 TMG:<br />
                              An der Prinzenmauer 55<br />
                              35510 Butzbach<br />
                              Vertreten durch:<br />
                              Benjamin Bilski<br />
                              Björn Scheurich<br />
                              Alexander Braune<br />
                              <br />
                          </p>
                      </div>
                      <!--/ .widget-->

                      <div class="widget widget_contacts opacity">
                          <ul class="contact-details">
                              <li>Adresse: Swipy Shopping GbR</li>
                              <li>Tel: 06033 67954</li>
                              <li>E-Mail: <a class="mail-link" href="mailto: ben@swipy.de">ben@swipy.de</a></li>
                          </ul>
                          <!--/ .contact-details-->

                      </div>
                      <!--/ .widget-->



                  </div>
              </div>
              <!--/ .row-->

          </div>
          <!--/ .container-->

      </section>
      <!--/ .section-->
  </section>
  <!--/ .page-->

  <section id="policy" class="page">
    <section class="section border bg-gray-color">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <hgroup class="section-title align-center opacity">
              <h1>Datenschutz</h1>
            </hgroup>
          </div>
        </div>
        <!--/ .row-->

      </div>
      <!--/ .container-->

      <div class="container">

        <p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.
        Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.</p>

        <p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.</p>

        <p><strong>Datenschutzerklärung für die Nutzung von Facebook-Plugins (Like-Button)</strong></p>

        <p>Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook (Facebook Inc., 1601 Willow Road, Menlo Park, California, 94025, USA) integriert. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem "Like-Button" ("Gefällt mir") auf unserer Seite. Eine Übersicht über die Facebook-Plugins finden Sie hier: <a href="http://developers.facebook.com/docs/plugins/" class="mail-link">http://developers.facebook.com/docs/plugins/</a> .
        Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser und dem Facebook-Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer IP-Adresse unsere Seite besucht haben. Wenn Sie den Facebook "Like-Button" anklicken während Sie in Ihrem Facebook-Account eingeloggt sind, können Sie die Inhalte unserer Seiten auf Ihrem Facebook-Profil verlinken. Dadurch kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Facebook erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von facebook unter <a href="http://de-de.facebook.com/policy.php" class="mail-link">http://de-de.facebook.com/policy.php</a> <br />
        Wenn Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook-Nutzerkonto zuordnen kann, loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.</p>

        <p><strong>Datenschutzerklärung für die Nutzung von Google Analytics</strong></p>

        <p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.<br />
        Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.<br />
        Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de" class="mail-link">http://tools.google.com/dlpage/gaoptout?hl=de</a> .<br />

        <p><strong>Datenschutzerklärung für die Nutzung von Google Adsense</strong></p>

        <p>Diese Website benutzt Google AdSense, einen Dienst zum Einbinden von Werbeanzeigen der Google Inc. ("Google"). Google AdSense verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website ermöglicht. Google AdSense verwendet auch so genannte Web Beacons (unsichtbare Grafiken). Durch diese Web Beacons können Informationen wie der Besucherverkehr auf diesen Seiten ausgewertet werden.<br />
        Die durch Cookies und Web Beacons erzeugten Informationen über die Benutzung dieser Website (einschließlich Ihrer IP-Adresse) und Auslieferung von Werbeformaten werden an einen Server von Google in den USA übertragen und dort gespeichert. Diese Informationen können von Google an Vertragspartner von Google weiter gegeben werden. Google wird Ihre IP-Adresse jedoch nicht mit anderen von Ihnen gespeicherten Daten zusammenführen.<br />
        Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.<br />
        Datenschutzerklärung für die Nutzung von Google +1</p>

        <p><strong>Erfassung und Weitergabe von Informationen:</strong></p>

        <p>Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit veröffentlichen. Über die Google +1-Schaltfläche erhalten Sie und andere Nutzer personalisierte Inhalte von Google und unseren Partnern. Google speichert sowohl die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre +1 können als Hinweise zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet werden.<br />
        Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere zu verbessern. Um die Google +1-Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares, öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser Name wird in allen Google-Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität Ihres Google-Profils kann Nutzern angezeigt werden, die Ihre E-Mail-Adresse kennen oder über andere identifizierende Informationen von Ihnen verfügen.</p>

        <p><strong>Verwendung der erfassten Informationen:</strong></p>

        <p>Neben den oben erläuterten Verwendungszwecken werden die von Ihnen bereitgestellten Informationen gemäß den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht möglicherweise zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw. gibt diese an Nutzer und Partner weiter, wie etwa Publisher, Inserenten oder verbundene Websites.<br />
        Datenschutzerklärung für die Nutzung von Twitter<br />
        Auf unseren Seiten sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden angeboten durch die Twitter Inc., Twitter, Inc. 1355 Market St, Suite 900, San Francisco, CA 94103, USA. Durch das Benutzen von Twitter und der Funktion "Re-Tweet" werden die von Ihnen besuchten Webseiten mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an Twitter übertragen.<br />
        Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Twitter unter <a class="mail-link" href="http://twitter.com/privacy">http://twitter.com/privacy</a>.<br />
        Ihre Datenschutzeinstellungen bei Twitter können Sie in den Konto-Einstellungen unter <a href="http://twitter.com/account/settings" class="mail-link">http://twitter.com/account/settings</a> ändern.

        <p>„Quellverweis: eRecht24, Facebook-Disclaimer von eRecht24, Datenschutzerklärung Google Analytics, eRecht24 Datenschutzerklärung GoogleAdsense, Datenschutzerklärung für Google +1, Datenschutzerklärung Twitter"</p>


      </div>
    </section>
    <!--/ .section-->

  </section>
  <!--/ .page-->
  
  <section id="imprint" class="page">
    <section class="section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <hgroup class="section-title align-center opacity">
              <h1>Impressum</h1>
            </hgroup>
          </div>
        </div>
        <!--/ .row--> 
        
      </div>
      <!--/ .container--> 
      <div class="container">


        <p><strong>Angaben gemäß § 5 TMG:</strong></p>

        <p>Swipy Shopping GbR<br />
          An der Prinzenmauer 55<br />
          35510 Butzbach</p>

        <p>Vertreten durch:<br />
          Benjamin Bilski<br />
          Björn Scheurich<br />
          Alexander Braune</p>

        <ul class="list circle-list opacityRun">

          <li>Kontakt: <a href="mailto: ben@swipy.de">ben@swipy.de</a></li>

          <li>Telefon: 06033 67954</li>

          <li>E-Mail: ben@swipy.de</li>

        </ul>


          <p class="align-center">Haftungsausschluss (Disclaimer)</p>


          <p><strong>Haftung für Inhalte</strong></p>

          <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich.
          Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>

          <p><strong>Haftung für Links</strong></p>

          <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>

          <p><strong>Urheberrecht</strong></p>

          <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>


      </div>
    </section>
    <!--/ .section-->
    

    
  </section>
  <!--/ .section-->
  

  
  <!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
  
  <footer id="footer">
    <div class="logo-in-footer">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1><a href="index.html">Swipy</a></h1>
          </div>
        </div>
        <!--/ .row--> 
        
      </div>
      <!--/ .container--> 
      
    </div>
    <!--/ .logo-in-footer-->
    
    <div class="bottom-footer clearfix">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="copyright"> Copyright &copy; 2014. <a target="_blank" href="index.html">Swipy</a>. All rights reserved </div>
            <!--/ .cppyright--> 
            
          </div>
          <div class="col-sm-3 col-sm-offset-3"> </div>
        </div>
        <!--/ .row--> 
        
      </div>
      <!--/ .container--> 
      
    </div>
    <!--/ .bottom-footer--> 
    
  </footer>
  <!--/ #footer--> 
  
  <!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - --> 
  
</div>
<!--/ #wrapper--> 

<!-- - - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - - --> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script> 
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
<![endif]--> 
<script src="js/jquery.queryloader2.js"></script> 
<script src="js/waypoints.min.js"></script> 
<script src="js/jquery.easing.1.3.min.js"></script> 
<script src="js/jquery.cycle.all.min.js"></script> 
<script src="js/layerslider/js/layerslider.transitions.js"></script> 
<script src="js/layerslider/js/layerslider.kreaturamedia.jquery.js"></script> 
<script src="js/jquery.mixitup.js"></script> 
<script src="js/jquery.mb.YTPlayer.js"></script> 
<script src="js/video-js/video.js"></script> 
<script>
    videojs.options.flash.swf = "js/video-js/video-js.swf";
  </script> 
<script src="js/jquery.smoothscroll.js"></script> 
<script src="js/flexslider/jquery.flexslider.js"></script> 
<script src="js/fancybox/jquery.fancybox.pack.js"></script> 
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/owl-carousel/owl.carousel.js"></script>
<script src="js/jquery.gmap.min.js"></script> 
<script src="js/jquery.touchswipe.min.js"></script> 
<script src="js/config.js"></script>
<script src="js/custom.js"></script>
<script src="js/rx.js.js"></script>
</body>
</html>
